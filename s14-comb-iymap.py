#!/usr/bin/env python
import random
import numpy as np
from astropy.io import fits as pyfits #version 0.4.2
import configparser as ConfigParser
import healpy as hp  #version 1.11.0
from scipy.linalg import svd
import os, time
import glob
from astropy.table import Table
import matplotlib.pyplot as plt
T_CMB = 2.7255
import matplotlib as mpl

cs = 16; csin = 16; 
mpl.rc('xtick', labelsize=cs) 
mpl.rc('ytick', labelsize=cs)
mpl.rcParams.update({'font.size': cs})
mpl.rcParams.update({'legend.fontsize': csin})

ymask = hp.read_map('../data/masks/ymask_p40.fits', field=0)
milca = hp.read_map('../data/ymap/milca_ymaps.fits', field=0)
nilc = hp.read_map('../data/ymap/nilc_ymaps.fits', field=0)

nside_sfilter = np.array([2,2,2,4,4,4,16,16,16,16]);

npix = int(hp.nside2npix(2048))
mapf = np.zeros(npix)
for I in range(10):

    if (1<=I and I<=2):
        indir = 'Outputs_cn6/'
        print(indir+'filt%d nside=%d' %(I,nside_sfilter[I]))
        ymapi = hp.read_map(indir+'milca_80_filt%d.fits' %(I), field=0)
    elif (3<=I and I<=5):
        indir = 'Outputs_cn6/'        
        print(indir+'filt%d nside=%d' %(I,nside_sfilter[I]))
        ymapi = hp.read_map(indir+'milca_40_filt%d.fits' %(I), field=0)
    elif (6<=I and I<=9):
        indir = 'Outputs_cn6/'        
        print(indir+'filt%d nside=%d' %(I,nside_sfilter[I]))
        ymapi = hp.read_map(indir+'milca_20_filt%d.fits' %(I), field=0)        
    else:
        print("None")
        ymapi = np.zeros(npix)

    mapf += ymapi
    

hp.write_map('ymaps/mymilca.fits', mapf, overwrite=True)

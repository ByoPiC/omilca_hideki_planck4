import numpy as np
from astropy.table import Table
import matplotlib.pyplot as plt
from astropy.io import fits

#lmax = 10000 - 1
lmax = 4096
ell = np.arange(lmax+1)+1.

indir = 'filter/'
f1 = fits.open(indir+'filters_4096_14_milca.fits')  # open a FITS file
tbdata1 = f1[1].data  # assume the first extension is a table
nfil1 = 14
bands1 = np.zeros((nfil1,4097))
for I in range(nfil1):
    bands1[I] = tbdata1.field('col%d' %(I));
    
f2 = fits.open(indir+'filters_nilc_4096_10.fits')  # open a FITS file
tbdata2 = f2[1].data  # assume the first extension is a table
nfil2 = 10
bands2 = np.zeros((nfil2,4097))
for I in range(nfil2):
    bands2[I] = tbdata2.field('col%d' %(I));

plt.figure()
for i in range(nfil1):
    plt.semilogx(ell,bands1[i],color = 'k')
for i in range(nfil2):
    plt.semilogx(ell,bands2[i],color = 'r')    

plt.xlabel('Multipole $\\ell$')
plt.ylabel('$B^{\\alpha}$')
plt.tight_layout()
plt.show()


plt.figure()
for i in range(nfil2-1):
    plt.semilogx(ell,bands2[i],color = 'k')    

plt.ylim(0,1)
plt.xlabel('Multipole $\\ell$')
plt.ylabel('${\it B}^{\\alpha}$')
plt.tight_layout()
plt.show()




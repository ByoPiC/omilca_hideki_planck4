#! /usr/bin/python
# coding: utf-8

import numpy as np
import healpy as hp
import matplotlib.pyplot as plt

Nside = 2048
indir = "/data/cluster/byopic/htanimur/data/Planck4/"

# Read data
MJy2Kcmb = np.array([244.0960,371.7327,483.6874,287.4517,58.0356,2.2681])
HFI100 = hp.read_map(indir+'HFI_SkyMap_100_2048_R4.00_full.fits', field=0)
HFI143 = hp.read_map(indir+'HFI_SkyMap_143_2048_R4.00_full.fits', field=0)
HFI217 = hp.read_map(indir+'HFI_SkyMap_217_2048_R4.00_full.fits', field=0)
HFI353 = hp.read_map(indir+'HFI_SkyMap_353_2048_R4.00_full.fits', field=0)
HFI545 = hp.read_map(indir+'HFI_SkyMap_545_2048_R4.00_full.fits', field=0)
HFI857 = hp.read_map(indir+'HFI_SkyMap_857_2048_R4.00_full.fits', field=0)

dipole = hp.read_map(indir+'dipole_nside2048.fits', field=0)    

HFI100_ud = HFI100 - dipole
HFI143_ud = HFI143 - dipole 
HFI217_ud = HFI217 - dipole
HFI353_ud = HFI353 - dipole
HFI545_ud = HFI545 - dipole
HFI857_ud = HFI857 - dipole

hp.write_map('HFI_SkyMap_100_2048_R4.00_%d.fits' %(Nside), HFI100_ud, overwrite=True)
hp.write_map('HFI_SkyMap_143_2048_R4.00_%d.fits' %(Nside), HFI143_ud, overwrite=True)
hp.write_map('HFI_SkyMap_217_2048_R4.00_%d.fits' %(Nside), HFI217_ud, overwrite=True)
hp.write_map('HFI_SkyMap_353_2048_R4.00_%d.fits' %(Nside), HFI353_ud, overwrite=True)
hp.write_map('HFI_SkyMap_545_2048_R4.00_%d.fits' %(Nside), HFI545_ud, overwrite=True)
hp.write_map('HFI_SkyMap_857_2048_R4.00_%d.fits' %(Nside), HFI857_ud, overwrite=True)

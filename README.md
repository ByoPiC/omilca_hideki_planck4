# OMILCA_Hideki_Planck4

(Meeting in 2020 Avril 29th)
Attendees: Hideki, Loïc

Loïc worked on refactoring the code

- fork of Hideki’s version fo the code, available on Gitlab: https://git.ias.u-psud.fr/lmaurin/omilca
ongoing work
Choice of the spatial regions

- Size (or nside) of the Healpix regions for each filter is chosen to reproduce the Planck y-map obtained by Guillaume. This is evaluated by comparing at PS level and 1D probability distribution.
After computing weights for each region, apply smoothing to reduce discontinuities at borders. Hideki asked Guillaume if it was better to smooth the coefficients or the resulting y-map: no clear answer. For now, the smoothing is done on coefficients.
Effect of the noise : MILCA on data or noise subtracted data ?

- No significant change.
Markov chain to minimize the noise has no significant effect and did not improve the y-map so Hideki does not use it.
Combining with ACT

Hideki's update

- Hideki made simulations with Websky of tSZ+CMB for ACT 98, 150 and Planck 100, 143, 217, 353, 545 convolved with beams
Project maps on BN region (rectangular maps) using pixell
filters in harmonic space (pixell is used to compute the alms for the flat sky) : 16 filters
Compute MILCA weights, the maps and PS
Relatively good agreement between PS. Wiggling at all scales. Overall differences are about 10% (above, below, above).
Next steps

Action

- [Loïc] Keep working on the refactoring of MILCA
- [Hideki] Apply the same processing to real Planck and ACT data
- [Hideki] Apply same filters to simulation but in full-sky to separate effects from filters and effects from projection/flat-sky


(Meeting in 2020 Avril 22th)
*Hideki's update:*

  * Explore the ACT BOSS-North data:
      o Footprint, noise, transfer function, window function
      o Point-source map detected  from ACT at a given threshold
      o Inverse variance map combination of four splits which used as a
        weight
      o CO-added map filtered from out for visualisation only,  the
        non-filter ones are used for science
  * Compute power spectra on ACT data: raw, filtered, half split; noise
    -> flat noise in agreement with publication
  *   Compute power spectra on Planck data


*Loic's updates:*

  * Forked MILCA
  * Run MILCA successfully and start to modify and re-write
  * Started some optimisation: unnecessary loops; minimize number of
    external calls to reduce human intervention; etc
  * Rewrite a code in the form of a library to substitute full to flat
    sky at will


*Actions/Next steps:*

  * Continue re-writing & cleaning the code with 2 version flat-Sky &
    full-sky
  * Easy way of combination: remove l<500 in ACT to remove the
    atmospheric effects
  * More complicated way of combination: filter the pow spect to remove
    the atmospheric effects and keep all the modes
  * Need to decide/investigate what filters to use and which space k and l
  * Decide the shape/areas of the patches related to pixelization



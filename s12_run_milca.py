#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Guillaume Hurier, Juan Macias-Perez, Marian Douspis"
__copyright__ = "Creative Commons"
__credits__ = ["Guillaume Hurier", "Juan Macias-Perez", "Marian Douspis", "Nabila Aghanim"]

__license__ = "CC"
__version__ = "2.0"
__year__ = "2018"
__maintainer__ = "M. Douspis"
__email__ = "marian.douspis@ias.u-psud.fr"

__Python_version__ = "3.5.2"
__Publication__= "http://www.aanda.org/articles/aa/abs/2013/10/aa21891-13/aa21891-13.html"

# Hideki messed up a lot and modified a little

import random
import numpy as np
from astropy.io import fits as pyfits #version 0.4.2
import configparser as ConfigParser
import healpy as hp  #version 1.11.0
from scipy.linalg import svd
import os, time
import glob
from astropy.table import Table
import matplotlib.pyplot as plt
import sys
argvs = sys.argv
print(argvs)
ifilt = int(argvs[1])
T_CMB = 2.7255

def milca_lect(fich):

    """
        Parser for the parameter file supplied in input.
        read the INI file containing inputs and settings for running MILCA
        
        Arg:
            fich: parameter file
        Returns:
            glist: List containing MILCA settings  
            mlist: List containing filenames of the input maps
            FF: Matrix containg frequency dependance of constrained components
            blist: List containing the FWHM of the beams for input maps 
            nlist: List containing filenames of the noise maps
            tlist: list containing Healpix format informations
            flist: list containing information on the filter
    """
    config = ConfigParser.ConfigParser()
    config.read(fich)
    options = config.options("MAPS")
    nop1 = len(options)
    mlist=[]
    for i in range(nop1):
        mlist.append(config.get("MAPS", options[i]))    
    alist=[]
    for i in range(nop1):
        alist.append(config.get("ALMS", options[i]))    
    nlist=[]
    for i in range(nop1):
        nlist.append(config.get("NOISE", options[i]))         
    nalist=[]
    for i in range(nop1):
        nalist.append(config.get("NALMS", options[i]))    
    blist=[]
    for i in range(nop1):
        blist.append(config.get("BEAMS", options[i]))    
    blist = np.double(blist)
    freqlist=[]
    for i in range(nop1):
        freqlist.append(config.get("FREQS", options[i]))    

    nop2 = int(config.get("GENERAL", "NCONST"))
    FF = np.matrix(np.zeros((nop2,nop1)))    
    for i in range(nop2):
        for j in range(nop1):
            tmp = config.get("CONST", "F"+str(i+1)+"_"+options[j][3]) 
            FF[i,j] = np.double(tmp)             

    mslist=[]
    for i in range(1):
        mslist.append(config.get("MASKMAP", options[i])) 
            
    tdict={}
    tdict["RING"]=config.get("HEALPIX", "RING")   
    tdict["NESTED"]=config.get("HEALPIX", "NESTED")   
    tdict["NSIDE"]=config.get("HEALPIX", "NSIDE")   
    tdict["LMAX"]=config.get("HEALPIX", "LMAX")    
    tdict["FWHM"]=config.get("HEALPIX", "FWHM")    
    
    fdict={}
    fdict["NUMBER"]=config.get("FILTERS", "NUMBER")
    fdict["FILE"]=config.get("FILTERS", "FILE")

    gdict={}
    gdict["NCONST"]=config.get("GENERAL", "NCONST")
    gdict["NASTR"]=config.get("GENERAL", "NASTR")
    gdict["NOISE"]=config.get("GENERAL", "NOISE")
    gdict["HEALPIX"]=config.get("GENERAL", "HEALPIX")
    gdict["ALMS"]=config.get("GENERAL", "ALMS")
    gdict["FILTERING"]=config.get("GENERAL", "FILTERING")
    gdict["INDIR"]=config.get("GENERAL", "INDIR")
    gdict["TEMPDIR"]=config.get("GENERAL", "TEMPDIR")
    gdict["OUTDIR"]=config.get("GENERAL", "OUTDIR")
    gdict["MASK"]=config.get("GENERAL", "MASK")
    
    Nmaps = nop1

    return gdict, mlist, alist, FF, blist, nlist, nalist, tdict, fdict, Nmaps, freqlist, mslist



def milca_alm_filter(almi, beam, tdict, fdict, filt, freq, noise=False):

    nside = int(tdict["NSIDE"])
    lmax = int(tdict["LMAX"])
        
    ell = np.array(np.arange(lmax+1))
    conv = np.pi/180.0/60.0/2.0/np.sqrt(2*np.log(2))
    
    if beam!=tdict["FWHM"]:
        bell = np.exp(-ell*(ell+1.0)*((np.double(tdict["FWHM"])**2-np.double(beam)**2)*conv**2)/2.0)
    else:
        bell = np.zeros(np.arange(lmax+1))+1.

    if(noise==False):
        tempo = glob.glob(gdict["TEMPDIR"]+"map_"+np.str(freq)+"_"+np.str(filt)+".fits")
        if(tempo==[]):
            fils, header = pyfits.getdata(fdict["FILE"],1,header=True)
            fil = fils.field(filt)[0:lmax+1]

            alm = hp.almxfl(almi,np.reshape(fil*bell,lmax+1))
            mapo = hp.alm2map(alm,nside,verbose=False)
            hp.write_map(gdict["TEMPDIR"]+"map_"+np.str(freq)+"_"+np.str(filt)+".fits", mapo)
        else:
            mapo = hp.read_map(tempo[0])
        return mapo

    elif(noise==True):
        tempo = glob.glob(gdict["TEMPDIR"]+"Nmap_"+np.str(freq)+"_"+np.str(filt)+".fits")
        if(tempo==[]):
            fils, header = pyfits.getdata(fdict["FILE"],1,header=True)
            fil = fils.field(filt)[0:lmax+1]
            
            alm = hp.almxfl(almi,np.reshape(fil*bell,lmax+1))
            mapo = hp.alm2map(alm,nside,verbose=False)
            hp.write_map(gdict["TEMPDIR"]+"Nmap_"+np.str(freq)+"_"+np.str(filt)+".fits", mapo)
        else:
            mapo = hp.read_map(tempo[0])
        return mapo

    else:
        print("No map output... strange")
        return np.nan
                
def milca_weights(cov,FF):

    invC = np.linalg.pinv(cov)
    FinvC = FF.dot(invC)
    den = FinvC.dot(FF.T)
    ww = (np.linalg.pinv(den)).dot(FinvC)

    return ww

def milca_comb(ww,alms,blist,tdict,fdict,filt):

    nf = len(blist)
    nside = int(tdict["NSIDE"])
    npix = np.int(12*nside**2)
    add = np.zeros(npix)
    for j in range(nf):
        
        tempm = glob.glob(gdict["TEMPDIR"]+"map"+str(j)+"_"+np.str(filt)+".fits")
        if tempm == []:
            map1 = milca_alm_filter(alms[j],blist[j],tdict,fdict,filt,j)
        else:
            map1 = hp.read_map(tempm[0])
                
        add += map1*ww[0,j]
    return add


# HT: Finally, I did not use this routine
def milca_mc(cov,cn,nmap,nn,FF):
    """
        markov chain to minimize the noise
        
        Args:
            cov: covariance matrix of input maps
            cn: covariance matrix of noise maps  
            nmap: number of input map
            nn: number of degree of freedom avaible for noise mimization
            FF: Matrix containg frequency dependance of constrained components
        Returns:
            cov: modified covariance matrix of input maps
    """
    U, Sold, V = np.linalg.svd(cov, full_matrices=1, compute_uv=1)
    k=0
    ww = milca_weights(cov,FF)
    nlev0 = (ww.dot(cn)).dot(ww.T)[0,0]
    random.seed()
    while k < 100:
        S = Sold
        S[nmap-nn:nmap] = Sold[nmap-nn:nmap] + np.random.normal(0,Sold[nmap-nn-1]*0.02,nn)
        cov = (U.dot(np.diag(S))).dot(V)
        ww = milca_weights(cov,FF)
        nlev = ((ww.dot(cn)).dot(ww.T))[0,0]
        if nlev <= nlev0:
            Sold = S
            nlev0 = nlev
            k = 0
        if nlev > nlev0:
            k += 1
    return cov                         






#############################################################
### Main 
#############################################################

fich = argvs[2]
w=True

gdict, mlist, alist, FF, blist, nlist, nalist, tdict, fdict, Nmaps, freqlist, mslist= milca_lect(fich)
nmap = Nmaps
nn = nmap-int(gdict["NCONST"])-int(gdict["NASTR"])
nside = int(tdict["NSIDE"])
npix = int(12*nside**2)
nfilt = int(fdict["NUMBER"])
mapf = np.zeros(npix)
nside = int(tdict["NSIDE"])
lmax = int(tdict["LMAX"])
if (gdict["NOISE"] == 'T'):
    noise = True
else:
    noise = False

if (gdict["MASK"] == 'T'):
    fmask = True
    mask = hp.read_map(os.path.join(gdict["INDIR"],mslist[0]))
else:
    fmask = False    



### Cal alms and nalms 
alms = []
for ifl,(almi,mapi) in enumerate(zip(alist,mlist)):

    tempalm = glob.glob(gdict["TEMPDIR"]+"alm*"+freqlist[ifl]+"*fits")
    print(ifl, almi, mapi, tempalm)

    if np.logical_and(almi=="", tempalm==[]):
        map1, header = hp.read_map(os.path.join(gdict["INDIR"],mapi),h=True)

        alm = hp.map2alm(map1)
        hp.write_alm(os.path.join(gdict["TEMPDIR"],"alm_"+mapi), alm)

    elif np.logical_and(almi=="", tempalm!=[]):
        alm = hp.read_alm(tempalm[0])

    elif np.logical_and(almi!="", tempalm==[]):
        alm = hp.read_alm(os.path.join(gdict["INDIR"],almi))
    else:
        print("2 alms ....")
        
    alms.append(alm)

if noise == True:
    nalms = []
    for ifl,(nalmi,nmapi) in enumerate(zip(alist,nlist)):

        tempalm = glob.glob(gdict["TEMPDIR"]+"Nalm*"+freqlist[ifl]+"*fits")
        print(ifl, nalmi, nmapi, tempalm)

        if np.logical_and(nalmi=="", tempalm==[]):
            nmap1, header = hp.read_map(os.path.join(gdict["INDIR"],nmapi),h=True)

            nalm = hp.map2alm(nmap1)
            hp.write_alm(os.path.join(gdict["TEMPDIR"],"Nalm_"+nmapi), nalm)
        
        elif np.logical_and(almi=="", tempalm!=[]):
            nalm = hp.read_alm(tempalm[0])

        elif np.logical_and(almi!="", tempalm==[]):
            nalm = hp.read_alm(os.path.join(gdict["INDIR"],nalmi))
        else:
            print("2 alms....")            

        nalms.append(nalm)


### Spatial filter
sfilter = np.zeros((nfilt,npix), dtype=np.int32)
nside_sfilter = np.array([2,2,2,4,4,4,16,16,16,16]);
npix_sfilter = np.zeros(nfilt, dtype=np.int32)

for ii in range(nfilt):

    npix_sfilter[ii] = int(12*nside_sfilter[ii]**2)
    sfilter_low = np.zeros(npix_sfilter[ii])

    for kk in range(hp.nside2npix(nside_sfilter[ii])):
        sfilter_low[kk] = kk+1

    sfilter[ii] = hp.ud_grade(sfilter_low,nside)

ww1_array = np.zeros((nmap,npix))

### Calc cov
for i in [ifilt]:

    ######
    filt = i
    nmap = Nmaps

    maps = []
    for jj in np.arange(nmap):
        mapjj = milca_alm_filter(alms[jj],blist[jj],tdict,fdict,filt,jj, noise=False)
        maps.append(mapjj)

    if noise == True:
        nmaps = []
        for jjj in np.arange(nmap):
            nmapjjj = milca_alm_filter(nalms[jjj],blist[jjj],tdict,fdict,filt,jjj, noise=True)
            nmaps.append(nmapjjj)

    ##### HT: Added to solve precision issue found for Victor's test (Websky)
    ### Initially done with cov mat such as (Cov dat - Cov noise)
    ### Changed to (map dat - map noise), then compute cov
    if noise == True:
        mapsn = np.array(maps) - np.array(nmaps)    
    ##################################################
        
    for sf in range(npix_sfilter[i]):

        if fmask == True:
            pix_effective = (sfilter[i]==(sf+1))*(mask==1)
        else:
            pix_effective = (sfilter[i]==(sf+1))
            
        print("%d out of %d" %(sf+1, npix_sfilter[i]))
        covm = np.zeros((nmap,nmap))
        covmn = np.zeros((nmap,nmap))        

        if noise == True:
            cn = np.zeros((nmap,nmap))
        
        for j in np.arange(nmap):

            covm[j][j] = np.mean((maps[j][pix_effective]-maps[j][pix_effective].mean())**2)

            if noise == True:
                covmn[j][j] = np.mean((mapsn[j][pix_effective]-mapsn[j][pix_effective].mean())**2)                
                cn[j][j] = np.mean((nmaps[j][pix_effective]-nmaps[j][pix_effective].mean())**2)                

            for k in np.arange(j+1,nmap):

                covm[k][j] = np.mean((maps[j][pix_effective]-np.mean(maps[j][pix_effective]))*(maps[k][pix_effective]-np.mean(maps[k][pix_effective])))
                covm[j][k] = covm[k][j]
                
                if noise == True:
                    covmn[k][j] = np.mean((mapsn[j][pix_effective]-np.mean(mapsn[j][pix_effective]))*(mapsn[k][pix_effective]-np.mean(mapsn[k][pix_effective])))
                    covmn[j][k] = covmn[k][j]
                
                    cn[k][j] = np.mean((nmaps[j][pix_effective]-np.mean(nmaps[j][pix_effective]))*(nmaps[k][pix_effective]-np.mean(nmaps[k][pix_effective])))
                    cn[j][k] = cn[k][j]

        covm = np.matrix(covm)
        ww = milca_weights(covm,FF)
        
        if noise == True:
            
            cn = np.matrix(cn)
            covmn = np.matrix(covmn)                
            ww = milca_weights(covmn,FF)

            #covmn = milca_mc(covmn,cn,nmaps,nn,FF):
            #ww = milca_weights(covmn,FF)
            
        pix_effective = (sfilter[i]==(sf+1))
        ww1_array[:nmap,pix_effective] = ww[0].T
        print("ww:",ww)

    np.save(gdict["OUTDIR"]+"ww1_%d_array.npy" %(ifilt), ww1_array)


#!/bin/bash
PROGRAM='s12_run_milca.py'

fich="./params/milca_params_cn6.ini"

for ifilt in 0 1 2 3 4 5 6 7 8 9
do
    echo $ifilt
    sbatch -J "Test Job" -N 1 -c 1 -p byopic --nodelist cluster-r730-4 ipython ${PROGRAM} ${ifilt} ${fich}
done

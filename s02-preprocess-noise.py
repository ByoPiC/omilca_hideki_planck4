#! /usr/bin/python
# coding: utf-8
import numpy as np
import healpy as hp
import matplotlib.pyplot as plt

Nside = 2048
indir = '/data/cluster/byopic/htanimur/data/Planck4/'

# Read data
MJy2Kcmb = np.array([244.0960,371.7327,483.6874,287.4517,58.0356,2.2681])
HFI100_1 = hp.read_map(indir+'HFI_SkyMap_100_2048_R4.00_full-ringhalf-1.fits', field=0)
HFI143_1 = hp.read_map(indir+'HFI_SkyMap_143_2048_R4.00_full-ringhalf-1.fits', field=0)
HFI217_1 = hp.read_map(indir+'HFI_SkyMap_217_2048_R4.00_full-ringhalf-1.fits', field=0)
HFI353_1 = hp.read_map(indir+'HFI_SkyMap_353_2048_R4.00_full-ringhalf-1.fits', field=0)
HFI545_1 = hp.read_map(indir+'HFI_SkyMap_545_2048_R4.00_full-ringhalf-1.fits', field=0)
HFI857_1 = hp.read_map(indir+'HFI_SkyMap_857_2048_R4.00_full-ringhalf-1.fits', field=0)

HFI100_2 = hp.read_map(indir+'HFI_SkyMap_100_2048_R4.00_full-ringhalf-2.fits', field=0)
HFI143_2 = hp.read_map(indir+'HFI_SkyMap_143_2048_R4.00_full-ringhalf-2.fits', field=0)
HFI217_2 = hp.read_map(indir+'HFI_SkyMap_217_2048_R4.00_full-ringhalf-2.fits', field=0)
HFI353_2 = hp.read_map(indir+'HFI_SkyMap_353_2048_R4.00_full-ringhalf-2.fits', field=0)
HFI545_2 = hp.read_map(indir+'HFI_SkyMap_545_2048_R4.00_full-ringhalf-2.fits', field=0)
HFI857_2 = hp.read_map(indir+'HFI_SkyMap_857_2048_R4.00_full-ringhalf-2.fits', field=0)

HFI100_noise_ud =  (HFI100_1 - HFI100_2)/2. 
HFI143_noise_ud =  (HFI143_1 - HFI143_2)/2. 
HFI217_noise_ud =  (HFI217_1 - HFI217_2)/2. 
HFI353_noise_ud =  (HFI353_1 - HFI353_2)/2. 
HFI545_noise_ud =  (HFI545_1 - HFI545_2)/2. #/ MJy2Kcmb[4]
HFI857_noise_ud =  (HFI857_1 - HFI857_2)/2. #/ MJy2Kcmb[5]

hp.write_map('HFI_SkyMap_100_2048_R4.00_N_%d.fits' %(Nside), HFI100_noise_ud, overwrite=True)
hp.write_map('HFI_SkyMap_143_2048_R4.00_N_%d.fits' %(Nside), HFI143_noise_ud, overwrite=True)
hp.write_map('HFI_SkyMap_217_2048_R4.00_N_%d.fits' %(Nside), HFI217_noise_ud, overwrite=True)
hp.write_map('HFI_SkyMap_353_2048_R4.00_N_%d.fits' %(Nside), HFI353_noise_ud, overwrite=True)
hp.write_map('HFI_SkyMap_545_2048_R4.00_N_%d.fits' %(Nside), HFI545_noise_ud, overwrite=True)
hp.write_map('HFI_SkyMap_857_2048_R4.00_N_%d.fits' %(Nside), HFI857_noise_ud, overwrite=True)



import numpy as np
from astropy.table import Table
import matplotlib.pyplot as plt

plt.ion()

lmax = 4096

### Needlet Window ###
fwhm = np.array([5.0,7.50,10.0000,13.4132,18.7716,25.2406,33.2659,43.5919,57.5805,78.0786,112.465,190.082,600.0,1200.0]) # [arcmin]
nfil = len(fwhm)+1
sigma = (fwhm/60.) * (np.pi/180.) / np.sqrt(8. * np.log(2.)) # [radian]

ell = np.arange(lmax+1)+1.
b = np.zeros((nfil-1,lmax+1)) # width
h = np.zeros((nfil,lmax+1)) # window

for j in range(nfil-1):
    b[j] = np.exp(-ell*(ell+1)*sigma[j]**2/2.)

b[0] = np.ones(lmax+1)
for j in range(nfil-2):
    h[j] = b[j] - b[j+1]

plt.figure()
for i in range(nfil):
    plt.semilogx(ell,h[i], color = 'k')
plt.ylim(0,1)
plt.xlabel(r'$\ell$')
plt.ylabel('Filter')
plt.show()

d = [h[i,:] for i in np.arange(nfil-1)]
filterss = Table(data=d) 
filterss.write("filter/filters_"+np.str(lmax)+"_"+np.str(len(d))+"_milca.fits", format="fits", overwrite=True)

#!/bin/bash
PROGRAM='s13-smth-wmap.py'

#fich="./params/milca_params_cn4.ini"
fich="./params/milca_params_nocn4.ini"

#fich="./params/milca_params_nocn5.ini"
#fich="./params/milca_params_nocn6.ini"
#fich="./params/milca_params_cn5.ini"
#fich="./params/milca_params_cn6.ini"

#for (( j=0; j<${NBIN}; j++ ))
for ifilt in 0 1 2 3 4 5 6 7 8 9
do
    echo $ifilt
    #sbatch -J "Test Job" -N 1 -c 1 -p byopic --nodelist cluster-r730-5 ipython ${PROGRAM} ${ifilt} ${fich}
    #sbatch -J "Test Job" -N 1 -c 1 -p all --nodelist cluster-r730-1 ipython ${PROGRAM} ${ifilt} ${fich}
    mpirun -np 1 ipython ${PROGRAM} ${ifilt} ${fich} &
done

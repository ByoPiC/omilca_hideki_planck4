#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Guillaume Hurier, Juan Macias-Perez, Marian Douspis"
__copyright__ = "Creative Commons"
__credits__ = ["Guillaume Hurier", "Juan Macias-Perez", "Marian Douspis", "Nabila Aghanim"]

__license__ = "CC"
__version__ = "2.0"
__year__ = "2018"
__maintainer__ = "M. Douspis"
__email__ = "marian.douspis@ias.u-psud.fr"

__Python_version__ = "3.5.2"
__Publication__= "http://www.aanda.org/articles/aa/abs/2013/10/aa21891-13/aa21891-13.html"


"""
TO be done MD 2018:
- gestion des masks
- propagation lmax, nside, etc dans les noms des alms et milca maps
- check on noise
- change alm2map in c to make all alms in the same time (current not faster)
- change alm2map to s2hat_alm2map for parallel
"""

import random
import numpy as np
from astropy.io import fits as pyfits #version 0.4.2
import configparser as ConfigParser
import healpy as hp  #version 1.11.0
from scipy.linalg import svd
import os, time
import glob
from astropy.table import Table
import matplotlib.pyplot as plt
T_CMB = 2.7255
import sys
argvs = sys.argv
print(argvs)
ifilt = int(argvs[1])
fich = argvs[2]

def milca_lect(fich):
    #print("")    
    #print("milca_lect")

    """
        Parser for the parameter file supplied in input.
        read the INI file containing inputs and settings for running MILCA
        
        Arg:
            fich: parameter file
        Returns:
            glist: List containing MILCA settings  
            mlist: List containing filenames of the input maps
            FF: Matrix containg frequency dependance of constrained components
            blist: List containing the FWHM of the beams for input maps 
            nlist: List containing filenames of the noise maps
            tlist: list containing Healpix format informations
            flist: list containing information on the filter
    """
    config = ConfigParser.ConfigParser()
    config.read(fich)
    options = config.options("MAPS")
    nop1 = len(options)
    mlist=[]
    for i in range(nop1):
        mlist.append(config.get("MAPS", options[i]))    
    alist=[]
    for i in range(nop1):
        alist.append(config.get("ALMS", options[i]))    
    nlist=[]
    for i in range(nop1):
        nlist.append(config.get("NOISE", options[i]))         
    nalist=[]
    for i in range(nop1):
        nalist.append(config.get("NALMS", options[i]))    
    blist=[]
    for i in range(nop1):
        blist.append(config.get("BEAMS", options[i]))    
    blist = np.double(blist)
    freqlist=[]
    for i in range(nop1):
        freqlist.append(config.get("FREQS", options[i]))    

    nop2 = int(config.get("GENERAL", "NCONST"))
    FF = np.matrix(np.zeros((nop2,nop1)))    
    for i in range(nop2):
        for j in range(nop1):
            tmp = config.get("CONST", "F"+str(i+1)+"_"+options[j][3]) 
            FF[i,j] = np.double(tmp)             

    mslist=[]
    for i in range(1):
        mslist.append(config.get("MASKMAP", options[i])) 
            
    tdict={}
    tdict["RING"]=config.get("HEALPIX", "RING")   
    tdict["NESTED"]=config.get("HEALPIX", "NESTED")   
    tdict["NSIDE"]=config.get("HEALPIX", "NSIDE")   
    tdict["LMAX"]=config.get("HEALPIX", "LMAX")    
    tdict["FWHM"]=config.get("HEALPIX", "FWHM")    
    
    fdict={}
    fdict["NUMBER"]=config.get("FILTERS", "NUMBER")
    fdict["FILE"]=config.get("FILTERS", "FILE")

    gdict={}
    gdict["NCONST"]=config.get("GENERAL", "NCONST")
    gdict["NASTR"]=config.get("GENERAL", "NASTR")
    gdict["NOISE"]=config.get("GENERAL", "NOISE")
    gdict["HEALPIX"]=config.get("GENERAL", "HEALPIX")
    gdict["ALMS"]=config.get("GENERAL", "ALMS")
    gdict["FILTERING"]=config.get("GENERAL", "FILTERING")
    gdict["INDIR"]=config.get("GENERAL", "INDIR")
    gdict["TEMPDIR"]=config.get("GENERAL", "TEMPDIR")
    gdict["OUTDIR"]=config.get("GENERAL", "OUTDIR")
    gdict["MASK"]=config.get("GENERAL", "MASK")
    
    Nmaps = nop1

    return gdict, mlist, alist, FF, blist, nlist, nalist, tdict, fdict, Nmaps, freqlist, mslist



def milca_alm_filter(almi, beam, tdict, fdict, filt, freq, noise=False):

    nside = int(tdict["NSIDE"])
    lmax = int(tdict["LMAX"])
        
    ell = np.array(np.arange(lmax+1))
    conv = np.pi/180.0/60.0/2.0/np.sqrt(2*np.log(2))
    
    if beam!=tdict["FWHM"]:
        bell = np.exp(-ell*(ell+1.0)*((np.double(tdict["FWHM"])**2-np.double(beam)**2)*conv**2)/2.0)
    else:
        bell = np.zeros(np.arange(lmax+1))+1.

    if(noise==False):
        tempo = glob.glob(gdict["TEMPDIR"]+"map_"+np.str(freq)+"_"+np.str(filt)+".fits")
        if(tempo==[]):
            fils, header = pyfits.getdata(fdict["FILE"],1,header=True)
            fil = fils.field(filt)[0:lmax+1]

            alm = hp.almxfl(almi,np.reshape(fil*bell,lmax+1))
            mapo = hp.alm2map(alm,nside,verbose=False)
            hp.write_map(gdict["TEMPDIR"]+"map_"+np.str(freq)+"_"+np.str(filt)+".fits", mapo)
        else:
            mapo = hp.read_map(tempo[0])
        return mapo

    elif(noise==True):
        tempo = glob.glob(gdict["TEMPDIR"]+"Nmap_"+np.str(freq)+"_"+np.str(filt)+".fits")
        if(tempo==[]):
            fils, header = pyfits.getdata(fdict["FILE"],1,header=True)
            fil = fils.field(filt)[0:lmax+1]

            alm = hp.almxfl(almi,np.reshape(fil*bell,lmax+1))
            mapo = hp.alm2map(alm,nside,verbose=False)
            hp.write_map(gdict["TEMPDIR"]+"Nmap_"+np.str(freq)+"_"+np.str(filt)+".fits", mapo)
        else:
            mapo = hp.read_map(tempo[0])
        return mapo

    else:
        print("No map output... strange")
        return np.nan


def milca_weights(cov,FF):

    invC = np.linalg.pinv(cov)
    FinvC = FF.dot(invC)
    den = FinvC.dot(FF.T)
    ww = (np.linalg.pinv(den)).dot(FinvC)

    return ww

def milca_comb(ww,alms,blist,tdict,fdict,filt):

    nf = len(blist)
    nside = int(tdict["NSIDE"])
    npix = np.int(12*nside**2)
    add = np.zeros(npix)
    for j in range(nf):
        
        tempm = glob.glob(gdict["TEMPDIR"]+"map_"+str(j)+"_"+np.str(filt)+".fits")
        print("map:", tempm)
        
        if tempm == []:
            map1 = milca_alm_filter(alms[j],blist[j],tdict,fdict,filt,j)
        else:
            map1 = hp.read_map(tempm[0])
                
        add += map1*ww[j]

    return add








#############################################################
#############################################################
#############################################################
### Main 
#############################################################

w=True


gdict, mlist, alist, FF, blist, nlist, nalist, tdict, fdict, Nmaps, freqlist, mslist= milca_lect(fich)
nmap = Nmaps
nn = nmap-int(gdict["NCONST"])-int(gdict["NASTR"])
nside = int(tdict["NSIDE"])
npix = int(12*nside**2)
nfilt = int(fdict["NUMBER"])

nside = int(tdict["NSIDE"])
lmax = int(tdict["LMAX"])

if (gdict["NOISE"] == 'T'):
    noise = True
else:
    noise = False

if (gdict["MASK"] == 'T'):
    fmask = True
    mask = hp.read_map(os.path.join(gdict["INDIR"],mslist[0]))
else:
    fmask = False    


### Cal alms and nalms 
alms = []
nalms = []

nside_sfilter = np.array([2,2,2,4,4,4,16,16,16,16]);
npix_sfilter = np.zeros(nfilt, dtype=np.int32)            
smths = np.array([80,80,80,40,40,40,20,20,20,20]);

mapf = np.zeros(npix)
mapf_smth1 = np.zeros(npix)

fmask = False
print("fmask:", fmask)

### Calc cov 
for i in [ifilt]:
    
    ######
    filt = i
    nmap = Nmaps

    ww = np.load(gdict["OUTDIR"]+"ww1_"+str(i)+"_array.npy")    
    ww_smth1 = np.zeros((nmap,npix))

    for j in range(nmap):

        if (i<6):
            ww_smth1[j] = hp.smoothing(ww[j],fwhm=np.radians(smths[i]))

        elif (6<=i and i<10):
            ww_smth1[j] = hp.smoothing(ww[j],fwhm=np.radians(smths[i]))
        else:
            ww_smth1[j] = hp.smoothing(ww[j],fwhm=np.radians(smths[i]))


    mapi_ww_smth1 = milca_comb(ww_smth1,alms,blist,tdict,fdict,i)
    mapf_smth1 += mapi_ww_smth1

    np.save(gdict["OUTDIR"]+"ww1_smth_%d_array.npy" %(filt), ww_smth1)
    
    hp.write_map(gdict["OUTDIR"]+"milca_%d_filt%d.fits" %(smths[i],filt), mapi_ww_smth1, overwrite=True)


